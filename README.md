
# API jobboard

## Description



## Technique

### Installation

Commandes en CLI, installations :    
*  `npm install` : Installer ou mettre à jour NPM    
*  `npm init` puis remplir les champs demandés s'il y a lieu : NPM. 'entry point' : server.js    
*  `npm install --save express` : Framework Express    
*  `npm install --save body-parser` : body-parser pour récupérer les différents champs du body    
*  `npm install --save cors` : cors pour permettre d'accéder aux url depuis la même origine    
*  `npm install --save-dev nodemon` : nodemon pour éviter d'avoir à relancer le server (node) à chaque changement. On n'en a besoin qu'en phase de dev.    
*  `npm install --save mysql2` : mysql2 pour gérer la bdd    
*  `npm install --save sequelize` : sequelize pour utiliser des méthodes déjà configurées pour les requêtes à la bdd    
*  `npm install --save nodemailer` : paquet qui gère l'envoi de mails
*  `npm install dotenv` : pour utiliser un fichier .env afin d'externaliser les données sensibles et ne pas les faire suivre par Git, et définir un environnement (développement / test / production)
*  `npm install --save bcrypt` : sert à hasher le mot de passe et le comparer avec celui rentré dans un formulaire
*  `npm install --save jsonwebtoken` : créer des tokens d'authentification



### Tests : utilisation de Postman

Pour tester Instance.create :    
**Method** : POST    
**URL** localhost:3000/< route/vers/méthode >    
**Elément de la requête** : Body    
**Forme** : Raw    
**Syntaxe** : JSON    
**Contenu**:    
`    {`    
`       "title": "offer bb",`    
`       "fk_enterprise_id": 2`    
`    }`    


### Cheatsheets ###    

**NPM**    
`npm list` : obtenir la liste des packets installés et leur version        
`npm list < paquet >` : obtenir la version du packet indiqué    


**GIT**
`git branch` ou `git branch --list` : liste toutes les branches existantes    
`git remote -v` : liste les repos distants

**Sequelize**    
(ToFix : like %keyword% ne fonctionne pas : récupère le dernier, quoi qu'il arrive.)        
*  Opérateurs de recherche :    
Pour rechercher des enregistrements utilisant des opérateurs (like, equal, greater than, and, or...), on utilise le module Sequelize.Op. Il faut donc importer Sequelize et déclarer la constante Op dans le controller :    
`const Sequelize = requeire('sequelize');`    
`const Op = Sequelize.Op;`


**Bcrypt**

Lien utile : 
[A quick guide for Authentication using ‘bcrypt’ on Express/NodeJs](https://medium.com/@mridu.sh92/a-quick-guide-for-authentication-using-bcrypt-on-express-nodejs-1d8791bb418f)













