
//  importer le module express et 
//  définir app comme objet express()
const express = require('express');
const app = express();

const bodyParser = require('body-parser');

//  importer la config du fichier .env
require('dotenv').config()

//  importer routes
const offerRoutes = require('./routes/offerRoutes');
const enterpriseRoutes = require('./routes/enterpriseRoutes');
const pagesRoutes = require('./routes/pagesRoutes');
const userRoutes = require('./routes/userRoutes');
const candidateRoutes = require('./routes/candidateRoutes');
const candOfferRoutes = require('./routes/candOfferRoutes');
const roleRoutes = require('./routes/roleRoutes');
const sectorController = require('./routes/sectorRoutes');
const statusController = require('./routes/statusRoutes.Js');
const contractController = require('./routes/contractRoutes');
const contactController = require('./routes/contactRoute');


//  CORS pour "contourner" le same origin policy
const cors = require('cors');
const corsOptions = {
    origin: '*'
}
app.use(cors(corsOptions));

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

//  utilisation des fichiers de routes selon l'URI
app.use('/candidate', candidateRoutes);
app.use('/candOffer', candOfferRoutes);
app.use('/enterprise', enterpriseRoutes);
app.use('/offer', offerRoutes);
app.use('/pages', pagesRoutes);
app.use('/user', userRoutes);
app.use('/role', roleRoutes);
app.use('/sector', sectorController);
app.use('/status', statusController);
app.use('/contract', contractController);
app.use('/email', contactController);


//  export du module app
module.exports = app;

