
const Staticpages = require('../models/staticpagesModel');

const all_pages = (req, res) => {
  Staticpages.findAll({}).then((staticpages) => {
    console.log(staticpages);
    res.send(staticpages);
  }).catch((err) => {
    console.log(err);
  })
};

const one_page = (req, res) => {
  const id = req.params.id;
  Staticpages.findByPk(id, {}).then((staticpage) => {
    console.log(staticpage);
    res.send(staticpage);
  }).catch((err) => {
    console.log(err);
  })
};

const create_page = (req, res) => {
  Staticpages.create({
    title: req.body.title,
    content: req.body.content,
    image: req.body.image,
    is_active: req.body.image
  }).then(() => {
    res.status(200).send({
      message: "Page enregistrée"
    });
  }).catch((err) => {
    console.log(err);
  })
};

const update_page = (req, res) => {
  const id = req.params.id;
  console.log(id);
  Staticpages.update({
    title: req.body.title,
    content: req.body.content,
    image: req.body.image,
    is_active: req.body.image
  }, {
    where: {
      static_id: id
    }
  }).then(() => {
    res.status(200).send({
      message: "Modifications enregistrées"
    })
  }).catch((err) => {
    console.log(err);
  })
};

//  désactiver une page
const delete_page = (req, res) => {
  const id = req.params.id;
  Staticpages.destroy({
    is_active: 0
  }, {
    where: {
      static_id: id
    }
  }).then(() => {
    res.status(200).send({
      message: "Page supprimée"
    })
  }).catch((err) => {
    console.log(err);
  })
};



//  exports des méthodes
exports.all_pages = all_pages;
exports.one_page = one_page;
exports.create_page = create_page;
exports.update_page = update_page;
exports.delete_page = delete_page;

