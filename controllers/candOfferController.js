
const CandOffer = require('../models/candOfferModel');
const Candidate = require('../models/candidateModel');
const candidateModel = require('../models/candidateModel');
const Offer = require('../models/offerModel');
const offerModel = require('../models/offerModel');


//  créer une relation entre un candidat et une offer
//  envoi d'un objet
//  réponse si statut 200 "ok" : object "message"
const create_candOffer = (req, res) => {
  CandOffer.create({
    fk_candidate_id: req.body.fk_candidate_id,
    fk_offer_id: req.body.fk_offer_id 
  }).then(() => {
    res.status(200).send({
      message: "Relation enregistrée"
    })
  }).catch((err) => {
    console.log(err);
  })
};


//  récupérer toutes les relations candidat-offre
//  réponse sous forme d'un tableau d'objets
const all_candOffers = (req, res) => {
  CandOffer.findAll({
    include: [{
      model: candidateModel,
      key: 'candidate_id'
    }, {
      model: offerModel,
      key: 'offer_id'
    }], 
    order: [
      ['candoffer_id', 'DESC']
    ]
  }).then((candOffers) => {
    console.log(candOffers);
    res.send(candOffers);
  }).catch((err) => {
    console.log(err);
  })
};

//  récupérer une relation
//  réponse : un objet
const one_candOffer = (req, res) => {
  const id = req.params.id;
  CandOffer.findByPk(id, {
    include: [{
      model: candidateModel,
      key: 'candidate_id'
    }, {
      model: offerModel,
      key: 'offer_id'
    }]
  }).then((candOffer) => {
    console.log(candOffer);
    res.send(candOffer);
  }).catch((err) => {
    console.log(err);
  })
};

//  modifier une relation
//  on envoie les données sous forme d'un objet
//  réponse si statut 201 : un objet "message"
const update_candOffer = (req, res) => {
  const id = req.params.id;
  CandOffer.update({
    fk_candidate_id: req.body.fk_candidate_id,
    fk_offer_id: req.body.fk_offer_id
  }, {
    where: {
      candoffer_id: id
    }
  }).then(() => {
    res.status(201).send({
      message: "Modifications enregistrées"
    })
  }).catch((err) => {
    console.log(err);
  })
};


//  désactiver une relation
//  Voir comment supprimer FK en même temps que destroy, sinon desactive à la place
const deactivate_candOffer = (req, res) => {
  const id = req.params.id;
  const is_active = req.body.is_active;
  CandOffer.findByPk(id).then((candOffer) => {
    candOffer.is_active = is_active;
    candOffer.save();
    res.status(201).send({
      message: 'Modification enregistrée;'
    })
  })
};
  // CandOffer.update({
  //   is_active: 0
  // }, {
  //   where: {
  //     candoffer_id: id
  //   }
  // }).then(() => {
  //   res.status(201).send({
  //     message: "Relation désactivée"
  //   })
  // }).catch((err) => {
  //   console.log(err);
  // })


//  supprimer la relation
//  envoi de l'id en param
//  réponse : objet message en fct du statut renvoyé
// const delete_candOffer = (req, res) => {
//   const id = req.params.id;
//   CandOffer.destroy({
//     where: {
//       candoffer_id: id
//     }
//   }).then(() => {
//     res.status(201).send(candoffer_id = {} ? {message: "Aucune relation avec cet ID"} : "Relation supprimée");
//     // .send({
//     //   message: "Relation supprimée"
//     // })
//   }).catch((err) => {
//     console.log(err);
//   })
// };


/*********************************************************************/
/******************* CANDIDATES BY OFFER *****************************/
/*********************************************************************/

const candidates_by_offer = (req, res) => {
  const offer = req.params.offer;
  CandOffer.findAll({
    include: [{
      model: candidateModel,
      required: true
    }, {
      model: offerModel,
      required: true
    }], 
    where: {
      fk_offer_id: offer
    }, 
    order: [
      ['candoffer_id', 'DESC']
    ]
  }).then((cand_offer) => {
    console.log(cand_offer);
    res.send(cand_offer);
  }).catch((err) => {
    console.log(err);
  })
};


/*********************************************************************/
/******************* OFFERS BY CANDIDATE *****************************/
/*********************************************************************/

const offers_by_candidate = (req, res) => {
  const candidate = req.params.candidate;
  CandOffer.findAll({
    include: [{
      model: candidateModel,
      required: true
    }, {
      model: offerModel,
      required: true
    }], 
    where: {
      fk_candidate_id: candidate
    }, 
    order: [
      ['candoffer_id', 'DESC']
    ]
  }).then((offer_cand) => {
    console.log(offer_cand);
    res.send(offer_cand);
  }).catch((err) => {
    console.log(err);
  })
};


//  exports des méthodes

exports.create_candOffer = create_candOffer;
exports.all_candOffers = all_candOffers;
exports.one_candOffer = one_candOffer;
exports.update_candOffer = update_candOffer;
exports.deactivate_candOffer = deactivate_candOffer;
// exports.delete_candOffer = delete_candOffer;

exports.candidates_by_offer = candidates_by_offer;
exports.offers_by_candidate = offers_by_candidate;



