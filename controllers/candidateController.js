
const Candidate = require('../models/candidateModel');
const User = require('../models/userModel');


const create_candidate = (req, res) => {
  Candidate.create({
    fk_user_id: req.body.fk_user_id,
    cv: req.body.cv
  }).then(() => {
    res.status(200).send({
      message: "Candidat enregistré"
    });
  }).catch((err) => {
    console.log(err);
  })
};


//  récupérer tous les candidats
//  réponse sous forme d'un tableau d'objets
const all_candidates = (req, res) => {
  Candidate.findAll({
    //  afficher les relations sous la forme d'un tableau d'objets
    include: [{
      model: User,
      required: true
    }],
    order: [
      ['candidate_id', 'DESC']
    ]
  }).then((candidates) => {
    console.log(candidates);
    res.send(candidates);
  }).catch((err) => {
    console.log(err);
  })
};

//  Récupérer un candidat par son ID
//  Si résultat vide : message
//  réponse : les données sous forme d'objet
const one_candidate = (req, res) => {
  const id = req.params.id;
  Candidate.findByPk(id, {
    include: [{
      model: User,
      required: true
    }]
  }).then((candidate) => {
    console.log(candidate);
    res.send(candidate);
  }).catch((err) => {
    console.log(err);
  })
};

//  candidate by user
//  req : select * from candidate where fk_user_id = req.params.user
const cand_by_user = (req, res) => {
  const user = req.params.user;
  console.log(req.params);
  Candidate.findOne({
    include: [{
      model: User,
      required: true
    }],
    where: {
      fk_user_id: user
    }
  }).then((ent_user) => {
    console.log(ent_user);
    res.send(ent_user);
  }).catch((err) => {
    console.log(err);
  });
};


//  Modifier un enregistrement :
//  envoi des données sous forme d'un objet
//  réponse sous forme d'objet "message"
const update_candidate = (req, res) => {
  const id = req.params.id;
  Candidate.update({
    fk_user_id: req.body.fk_user_id,
    cv: req.body.cv
  }, {
    where: {
      candidate_id: id
    }
  }).then(() => {
    res.status(201).send({
      message: "Modifications enregistrées"
    })
  }).catch((err) => {
    console.log(err);
  })
};


//  désactiver un candidat 
//  pour éviter le problème des relations
const deactivate_candidate = (req, res) => {
  const id = req.params.id;
  const is_active = req.body.is_active;
  Candidate.findByPk(id).then((candidate) => {
    candidate.is_active = is_active;
    candidate.save();
    res.status(201).send({
      message: 'Modification enregistrée'
    });
  }).catch((err) => {
    console.log(err);
  })
}

//  supprimer définitivement un candidat
//  réponse : objet "message"
// const delete_candidate = (req, res) => {
//   const id = req.params.id;
//   Candidate.destroy({
//     where: {
//       candidate_id: id
//     }
//   }).then(() => {
//     res.status(200).send({
//       message: "Candidat supprimé"
//     })
//   }).catch((err) => {
//     console.log(err);
//   })
// }

// CANDIDATES BY OFFER => dans candOfferController


//  export des méthodes comme modules
exports.create_candidate = create_candidate;
exports.all_candidates = all_candidates;
exports.one_candidate = one_candidate;
exports.cand_by_user = cand_by_user;
exports.update_candidate = update_candidate;
exports.deactivate_candidate = deactivate_candidate;
// exports.delete_candidate = delete_candidate;

