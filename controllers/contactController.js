
const nodemailer = require('nodemailer'); 

//  définition des variables utiles pour l'envoi du mail, depuis le fichier .env
const HOST = process.env.EMAIL_HOST;
const PORT = process.env.EMAIL_PORT;
const EMAIL_TO = process.env.EMAIL_TO;
const EMAIL_PASS = process.env.EMAIL_PASS;


// async..await is not allowed in global scope, must use a wrapper
const main = (req, res) => {

  //  récupérer les données du formulaire / front
  const from = req.body.from;
  const subject = req.body.subject;
  const message = req.body.message;
  const filename = req.body.filename;
  const path = req.body.path;

  // Generate test SMTP service account from ethereal.email
  // Only needed if you don't have a real mail account for testing
  // let testAccount = nodemailer.createTestAccount();

  //  récupérer les données du destinataire sur le fichier .env
  //  définir un transporteur pour le protocole SMTP
  let transporter = nodemailer.createTransport({
    host: HOST,
    port: PORT,
    secure: true,  // true for 465, false for other ports
    auth: {
      user: EMAIL_TO,  
      pass: EMAIL_PASS, 
    },
  });
  
  //  envoi du mail avec la config définie dans le .env
  let info = transporter.sendMail({
    from: from,
    to: EMAIL_TO, 
    subject: subject,
    text: message,
    attachments: [{   //  utiliser une url pour lier une pj
        filename: filename,
        path: path, 
        contentType: 'application/pdf'
      }]
  }).then((info) => {
    console.log("Message sent: %s", info.messageId);
    res.send({
      message: "Message envoyé : "+info.message
    })
  }).catch((err) => {
    console.log(err);
  })

}

exports.main = main;
