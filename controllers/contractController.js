
//  Import du modèle dont on aura besoin
const Contract = require('../models/contractModel');


//  créer un type de contrat dans la bdd
//  envoi d'un objet contrat{}
//  réponse : si 200 : objet "message"
//  sinon réponse : object "err"
const create_contract = (req, res) => {
  Contract.create({
    contract_type: req.body.contract_type
  }).then(() => {
    res.status(200).send({
      message: "Contrat enregistré"
    })
  }).catch((err) => {
    console.log(err);
  })
};


//  récupérer tous les types de contrat
//  envoi d'un objet par le protocole http get
//  réponse si statut 200 : tableau d'objets
//  si status autre : objet "err"
const all_contracts = (req, res) => {
  Contract.findAll({
  }).then((contracts) => {
    console.log(contracts);
    res.send(contracts);
  }).catch((err) => {
    console.log(err);
  })
};


//  récupérer un type de contrat
const one_contract = (req, res) => {
  const id = req.params.id;
  Contract.findByPk(id, {
  }).then((contract) => {
    console.log(contract);
    res.send(contract);
  }).catch((err) => {
    console.log(err);
  })
};


//  modifier un type de contract
//  envoi d'un objet contenant les nouvelles données
//  réponse si status 201 : un objet "message"
//  sinon un objet "err"
const update_contract = (req, res) => {
  const id = req.params.id;
  Contract.update({
    contract_type: req.body.contract_type
  }, {
    where:  {
      contract_id: id
    }
  }).then(() => {
    res.status(201).send({
      message: "Modifications enregistrées"
    }).catch((err) => {
      console.log(err);
    })
  })
};


//  exports des méthodes
exports.create_contract = create_contract;
exports.all_contracts = all_contracts;
exports.one_contract = one_contract;
exports.update_contract = update_contract;

