
//  import des modèles
const Enterprise = require('../models/enterpriseModel');
const Sector = require('../models/sectorModel');
const Status = require('../models/statusModel');
const User = require('../models/userModel');


//  get all enterprises
//  req : get select * from enterprise 
//  orderby enterprise_id desc
const all_enterprises = (req, res) => {
  Enterprise.findAll({
    order: [
      ['enterprise_id', 'DESC']
    ],
    include: [{
        model: Sector,
        required: true
      }, {
        model: Status,
        required: true
      }, {
        model: User,
        required: true
      }
    ],
    where: {
      is_active: 1
    }
  }).then((enterprises) => {
    console.log(enterprises);
    res.send(enterprises);
  }).catch((err) => {
    console.log(err);
  })
};


//  get one enterprises by id
//  req : get select * from enterprise where enterprise_id = req.params.id orderby enterprise_id desc
const one_enterprise = (req, res) => {
  const id = req.params.id;
  console.log(id);
  Enterprise.findByPk(id, {
    //  relations à afficher :
    include: [{
      model: Sector,
      required: true
    }, {
      model: Status,
      required: true
    }, {
      model: User,
      required: true
    }
  ]
  }).then((enterprise) => {
    console.log(enterprise);
    res.send(enterprise);
  }).catch((err) => {
    console.log(err);
  })
};


//  enterprise by user
//  req : select * from enterprise where fk_user_id = req.params.user
const ent_by_user = (req, res) => {
  const user_id = req.params.user_id;
  console.log(`user_id param : ${user_id}`);
  console.log(req.params);
  Enterprise.findOne({
    include: [{
      model: User,
      required: true
    }, {
      model: Sector, 
      required: true
    }, {
      model: Status,
      required: true
    }],
    where: {
      fk_user_id: user_id
    }
  }).then((ent_user) => {
    console.log(ent_user);
    res.send(ent_user);
  }).catch((err) => {
    console.log(err);
  });
};
/**************************** 
Offer.findAll({
  include: [{
    model: sectorModel, 
    required: true
  }, {
    model: statusModel,
    required: true
  }], 
  order: [
    ['offer_id', 'DESC']
  ]
}).
/************************** */

//  create enterprise
//  req : post insert enterprise (name, logo, description, size, website, fk_sector_id, fk_status_id, fk_user_id) values (req.body)
//  réponse statut et objet message si 200 ou erreur sinon
const create_enterprise = (req, res) => {
  Enterprise.create({
    name: req.body.name,
    logo: req.body.logo,
    description: req.body.description,
    size: req.body.size,
    website: req.body.website,
    fk_sector_id: req.body.fk_sector_id,
    fk_status_id: req.body.fk_status_id,
    fk_user_id: req.body.fk_user_id
  }).then(() => {
    res.status(200).send({
      message: 'Entreprise enregistrée'
    });
  }).catch((err) => {
    console.log(err);
  });
};

//  update_enterprise
//  req : put update enterprise set (name, logo, description, size, website, fk_sector_id, fk_status_id, fk_user_id) (req.body) where enterprise_id = req.params.id
//  réponse statut et objet message si 201 ou erreur sinon
const update_enterprise = (req, res) => {
  const id = req.params.id;
  Enterprise.update({
    name: req.body.name,
    logo: req.body.logo,
    fk_sector_id: req.body.fk_sector_id,
    fk_status_id: req.body.fk_status_id,
    fk_user_id: req.body.fk_user_id
  }, {
    where: {
      enterprise_id: id
    }
  }).then(() => {
    res.status(201).send({
      message: 'Modification enregistrée'
    });
  }).catch((err) => {
    console.log(err);
  });
};

//  deactivate_enterprise
//  req : put update enterprise set is_active à 0 where enterprise_id = req.params.id
//  réponse statut et objet message si 201 ou erreur sinon
const deactivate_enterprise = (req, res) => {
  const id = req.params.id;
  const is_active = req.body.is_active;
  Enterprise.findByPk(id).then((enterprise) => {
    enterprise.is_active = is_active;
    enterprise.save();
    res.status(201).send({
      message: 'Modification enregistrée'
    });
  }).catch((err) => {
    console.log(err);
  })
};
  // Enterprise.update({
  //   is_active: req.body.is_active
  // }, {
  //   where: {
  //     enterprise_id: id
  //   }
  // }).then(() => {
  //   res.status(201).send({
  //     message: 'Fiche entreprise désactivée.'
  //   });
  // }).catch((err) => {
  //   console.log(err);
  // });

//  delete_enterprise
// const delete_enterprise = (req, res) => {
//   const id = req.params.id;
//   Enterprise.destroy({
//     where: {
//       enterprise_id: id
//     }
//   }).then(() => {
//     res.status(200).send({
//       message: 'Fiche entreprise supprimée'
//     });
//   }).catch((err) => {
//     console.log(err);
//   })
// };
//  Impossible de supprimer une entreprise à cause des FK, voir comment les supprimer en même temps

/*************************************************************/
/******************** GET ENTERPRISES ************************/
/*************************************************************/

//  get enterprises by sector
const enterprises_by_sector = (req, res) => {
  const sector = req.params.sector;
  console.log(sector);
  Enterprise.findAll({
    //  relations
    include: [{
      model: Sector,
      required: true
    }, {
      model: Status,
      required: true
    }, {
      model: User,
      required: true
    }],
    where: {
      fk_sector_id: sector
    },
    order: [
      ['enterprise_id', 'DESC']
    ]
  }).then((enterprises_sector) => {
    console.log(enterprises_sector);
    res.send(enterprises_sector);
  }).catch((err) => {
    console.log(err);
  })
};

//  get one enterprises by status
const enterprises_by_status = (req, res) => {
  const status = req.params.status;
  console.log(status);
  Enterprise.findAll({
    //  relations
    include: [{
      model: Sector,
      required: true
    }, {
      model: Status,
      required: true
    }, {
      model: User,
      required: true
    }],
    where: {
      fk_status_id: status
    },
    order: [
      ['enterprise_id', 'DESC']
    ]
  }).then((enterprises_status) => {
    console.log(enterprises_status);
    res.send(enterprises_status);
  }).catch((err) => {
    console.log(err);
  })
};



/***************** exports des méthodes *************************/
exports.all_enterprises = all_enterprises;
exports.one_enterprise = one_enterprise;
exports.ent_by_user = ent_by_user;
exports.create_enterprise = create_enterprise;
exports.update_enterprise = update_enterprise;
exports.deactivate_enterprise = deactivate_enterprise;
//  exports.delete_enterprise = delete_enterprise;

exports.enterprises_by_sector = enterprises_by_sector;
exports.enterprises_by_status = enterprises_by_status;
