
//  imports offerModel, enterpriseModel 
const Offer = require('../models/offerModel');
const enterpriseModel = require('../models/enterpriseModel');
const statusModel = require('../models/statusModel');
const sectorModel = require('../models/sectorModel');
const contractModel = require('../models/contractModel');

//  pour pouvoir utiliser les opérateurs de Sequelize.Op :
const Sequelize = require('sequelize');
const Op = Sequelize.Op;


//  get all offers 
const all_offers = (req, res) => {
  Offer.findAll({
    include: [{
      model: contractModel,
      required: true
    }, {
      model: enterpriseModel,
      required: true,
    }, {
      model: sectorModel, 
      required: true
    }, {
      model: statusModel,
      required: true
    }], 
    order: [
      ['offer_id', 'DESC']
    ],
    where: {
      is_active: 1
    }
  }).then((offers) => {
    console.log(offers);
    res.send(offers);
  }).catch((err) => {
    console.log(err);
  });
};

//  get one offer by id 
const one_offer = (req, res) => {
  const id = req.params.id;
  console.log(id);
  Offer.findByPk(id, {
    include: [{
      model: contractModel,
      required: true
    }, {
      model: enterpriseModel,
      required: true,
    }, {
      model: sectorModel, 
      required: true
    }, {
      model: statusModel,
      required: true
    }], 
  }).then((offer) => {
    console.log(offer);
    res.send(offer);
  }).catch((err) => {
    console.log(err);
  });
};

//  create_offer
const create_offer = (req, res) => {
  Offer.create({
    title: req.body.title,
    fk_enterprise_id: req.body.fk_enterprise_id,
    detail: req.body.detail,
    recruitment_url: req.body.recruitment_url,
    offer_location: req.body.offer_location,
    xp_required: req.body.xp_required,
    fk_contract_id: req.body.fk_contract_id
  }).then(() => {
    res.status(200).send({
      message: 'Offre enregistrée'
    });
  }).catch((err) => {
    console.log(err);
  });
}

//  update_offer
const update_offer = (req, res) => {
  const id = req.params.id;
  Offer.update({
    title: req.body.title,
    fk_enterprise_id: req.body.fk_enterprise_id,
    detail: req.body.detail,
    recruitment_url: req.body.recruitment_url,
    offer_location: req.body.offer_location,
    xp_required: req.body.xp_required,
    fk_contract_id: req.body.fk_contract_id
  }, {
    where: {
      offer_id: id
    }
  }).then(() => {
    res.status(201).send({
      message: "modification enregistrée"
    });
  }).catch((err) => {
    console.log(err);
  });
};


//  deactivate_offer
const deactivate_offer = (req, res) => {
  const id = req.params.id;
  const is_active = req.body.is_active;
  Offer.findByPk(id).then((offer) => {
    offer.is_active = is_active;
    offer.save();
    res.status(201).send({
      message: 'Modification enregistrée'
    });
  })
};

//  supprimer l'offre
//  envoi id dans params
//  réponse objet message selon statut renvoyé
// exports.delete_offer = (req, res) => {
//   const id = req.params.id;
//   Offer.destroy({
//     where: {
//       offer_id: id
//     }
//   }).then(() => {
//     res.status(200).send(offer_id = {} ? { message: "Il n'y a pas d'offre avec cet ID" } : { message: "Offre supprimée" })
//   }).catch((err) => {
//     console.log(err);
//   })
// };
//  Voir comment supprimer les KF en même temps, sinon désactiver au lieu de supprimer

/************************************************/
/****************** FORM RECHERCHE **************/
/************************************************/


//  get offers by contract
//  envoi contract_id en param
//  réponse tableau d'objets
const get_offers_by_contract = (req, res) => {
  const contract = req.params.contract;
  Offer.findAll({
    order: [
      ['offer_id', 'DESC']
    ],
    include: [{
      model: enterpriseModel,
      required: true
    }, {
      model: contractModel,
      required: true
    }, {
      model: sectorModel,
      required: true
    }, {
      model: statusModel,
      required: true
    }],
    where: {
      fk_contract_id: contract
    }
  }).then((offers_contract) => {
    console.log(offers_contract);
    res.send(offers_contract);
    console.log(offers_contract.length);
  }).catch((err) => {
    console.log(err);
  })
};


//  get offers by ents
//  envoi sector en param
//  utilise la méthode ents_by_sector (sector du param)
//  réponse tableau de tableaux d'objets "resOffers"
const offers_by_ents = (req, res) => {
  const ent = req.params.ent;
  Offer.findAll({
    include: [{
      model: enterpriseModel,
      required: true,
    }, {
      model: sectorModel,
      required: true
    }, {
      model: statusModel,
      required: true
    }, {
      model: contractModel,
      required: true
    }],
    order: [
      ['offer_id', 'DESC']
    ],
    where: {
      fk_enterprise_id: ent
    }
  }).then((offers_ent) => {
    console.log(offers_ent);
    res.send(offers_ent);
  }).catch((err) => {
    console.log(err);
  })
};


//  get offers by sector
//  envoi sector_id en param
//  réponse tableau d'objets
const get_offers_by_sector = (req, res) => {
  const sector = req.params.sector;
  Offer.findAll({
    order: [
      ['offer_id', 'DESC']
    ],
    include: [{
      model: enterpriseModel,
      required: true,
    }, {
      model: sectorModel,
      required: true
    }, {
      model: statusModel,
      required: true
    }, {
      model: contractModel,
      required: true
    }],
    where: {
      fk_sector_id: sector
    }
  }).then((offers_sector) => {
    console.log(offers_sector);
    res.send(offers_sector);
  }).catch((err) => {
    console.log(err);
  })
};


//  get offers by status
//  envoi status en param
//  réponse tableau d'objets
const get_offers_by_status = (req, res) => {
  const status = req.params.status;
  Offer.findAll({
    order: [
      ['offer_id', 'DESC']
    ],
    include: [{
      model: enterpriseModel,
      required: true
    }, {
      model: contractModel, 
      required: true
    }, {
      model: sectorModel,
      required: true
    }, {
      model: statusModel,
      required: true
    }], 
    where: {
      fk_status_id: status
    }
  }).then((offers_status) => {
    console.log(offers_status);
    res.send(offers_status);
  }).catch((err) => {
    console.log(err);
  })
};


//  get offers by %keyword%
//  envoi keyword en param
//  réponse tableau d'objets
//  where (...) [Op.like]: '%'+kw+'%' insensible à la casse
const get_offers_by_keyword = (req, res) => {
  const kw = req.params.kw;
  Offer.findAll({
    include: [{
      model: contractModel,
      required: true
    }, {
      model: enterpriseModel,
      required: true,
    }, {
      model: sectorModel, 
      required: true
    }, {
      model: statusModel,
      required: true
    }], 
    order: [
      ['offer_id', 'DESC']
    ],
    where: {
      title: {
        [Op.like]: '%'+kw+'%'
      }
    }
  }).then((offers_kw) => {
    console.log(offers_kw);
    console.log(offers_kw.length);
    res.send(offers_kw);
  }).catch((err) => {
    console.log(err);
  })
};


//  get offers by many choices : 
//  formulaire de recherche
const get_offers_many_choices = (req, res) => {
  const kw = req.params.kw;
  const contract = req.params.contract;
  const sector = req.params.sector;
  const status = req.params.status;
  console.log(req.params[0]);
  Offer.findAll({
    //  jointures
    include: [{
      model: contractModel,
      required: true
    }, {
      model: enterpriseModel,
      required: true,
    }, {
      model: sectorModel, 
      required: true
    }, {
      model: statusModel,
      required: true
    }], 
    order: [
      ['offer_id', 'DESC']
    ],
    //  opérateur pour trier les 
    //  enregistrements à retourner
    where: {
      [Op.and]: [
        {title: { 
          [Op.like]: '%'+kw+'%'
        }},{
          [Op.or]: [
            {fk_contract_id: contract},
            {fk_sector_id: sector},
            {fk_status_id: status}
          ]
        }
      ]
    }
  }).then((offers) => {
    console.log(req.params);
    res.send(offers);
    console.log(offers.length);
  }).catch((err) => {
    console.log(err);
  })
};



/************** export des méthodes pour y diriger les routes ******************/

exports.all_offers = all_offers;
exports.one_offer = one_offer;
exports.create_offer = create_offer;
exports.update_offer = update_offer;
exports.deactivate_offer = deactivate_offer;

exports.get_offers_by_contract = get_offers_by_contract;
exports.offers_by_ents = offers_by_ents;
exports.get_offers_by_sector = get_offers_by_sector;
exports.get_offers_by_status = get_offers_by_status;
exports.get_offers_by_keyword = get_offers_by_keyword;
exports.get_offers_many_choices = get_offers_many_choices;
