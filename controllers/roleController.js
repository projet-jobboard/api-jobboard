
const Role = require('../models/roleModel');
const User = require('../models/userModel');


//  créer un role dans la bdd
//  envoi d'un objet
//  réponse si statut 200 "ok" : object "message"
//  sinon réponse object "err"
const create_role = (req, res) => {
  Role.create({
    title: req.body.title,
    description: req.body.description
  }).then(() => {
    res.status(200).send({
      message: "Rôle enregistré"
    })
  }).catch((err) => {
    console.log(err);
  })
};


//  récupérer tous les roles 
//  réponse sous forme d'un tableau d'objets
const all_roles = (req, res) => {
  Role.findAll({
    include: [{
      model: User,
      required: true
    }], 
    order: [
      ['user_id', 'DESC']
    ]
  }).then((roles) => {
    console.log(roles);
    res.send(roles);
  }).catch((err) => {
    console.log(err);
  })
};

//  récupérer un rôle
//  réponse : un objet
const one_role = (req, res) => {
  const id = req.params.id;
  Role.findByPk(id, {
    include: [{
      model: User,
      required: true
    }]
  }).then((role) => {
    console.log(role);
    res.send(role);
  }).catch((err) => {
    console.log(err);
  })
};

//  modifier un rôle
//  on envoie les données sous forme d'un objet
//  réponse si statut 201 : un objet "message"
const update_role = (req, res) => {
  const id = req.params.id;
  Role.update({
    title: req.body.title,
    description: req.body.description
  }, {
    where: {
      role_id: id
    }
  }).then(() => {
    res.status(201).send({
      message: "Modifications enregistrées"
    })
  }).catch((err) => {
    console.log(err);
  })
};


//  désactiver un rôle
//  Voir comment supprimer FK en même temps que destroy, sinon desactive à la place
const deactivate_role = (req, res) => {
  const id = req.params.id;
  const is_active = req.body.is_active;
  Role.findByPk(id).then((role) => {
    role.is_active = is_active;
    role.save();
    res.status(201).send({
      message: 'Modification enregistrée'
    });
  })
};
  // const id = req.params.id;
  // Role.update({
  //   is_active: 0
  // }, {
  //   where: {
  //     role_id: id
  //   }
  // }).then(() => {
  //   res.status(201).send({
  //     message: "Relation désactivée"
  //   })
  // }).catch((err) => {
  //   console.log(err);
  // })


//  exports des méthodes

exports.create_role = create_role;
exports.all_roles = all_roles;
exports.one_role = one_role;
exports.update_role = update_role;
exports.deactivate_role = deactivate_role;

