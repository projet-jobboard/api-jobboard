
//  Import du modèle dont on aura besoin
const Sector = require('../models/sectorModel');


//  créer un secteur dans la bdd
//  envoi d'un objet sector{}
//  réponse : si 200 : objet "message"
//  sinon réponse : object "err"
const create_sector = (req, res) => {
  Sector.create({
    sector_name: req.body.sector_name
  }).then(() => {
    res.status(200).send({
      message: "Secteur enregistré"
    })
  }).catch((err) => {
    console.log(err);
  })
};


//  récupérer tous les secteurs d'activité
//  envoi d'un objet par le protocole http get
//  réponse si statut 200 : tableau d'objets
//  si status autre : objet "err"
const all_sectors = (req, res) => {
  Sector.findAll({
  }).then((sectors) => {
    console.log(sectors);
    res.send(sectors);
  }).catch((err) => {
    console.log(err);
  })
};


//  récupérer un secteur d'activité
const one_sector = (req, res) => {
  const id = req.params.id;
  Sector.findByPk(id, {
  }).then((sector) => {
    console.log(sector);
    res.send(sector);
  }).catch((err) => {
    console.log(err);
  })
};


//  modifier un secteur
//  envoi d'un objet contenant les nouvelles données
//  réponse si status 201 : un objet "message"
//  sinon un objet "err"
const update_sector = (req, res) => {
  const id = req.params.id;
  Sector.update({
    sector_name: req.body.sector_name
  }, {
    where:  {
      sector_id: id
    }
  }).then(() => {
    res.status(201).send({
      message: "Modifications enregistrées"
    }).catch((err) => {
      console.log(err);
    })
  })
};








//  exports des méthodes

exports.create_sector = create_sector;
exports.all_sectors = all_sectors;
exports.one_sector = one_sector;
exports.update_sector = update_sector;

