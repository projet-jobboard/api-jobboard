
//  Import du modèle dont on aura besoin
const Status = require('../models/statusModel');


//  créer un statut dans la bdd
//  envoi d'un objet status{}
//  réponse : si 200 : objet "message"
//  sinon réponse : object "err"
const create_status = (req, res) => {
  Status.create({
    status_type: req.body.status_type
  }).then(() => {
    res.status(200).send({
      message: "Statut enregistré"
    })
  }).catch((err) => {
    console.log(err);
  })
};


//  récupérer tous les statuts
//  envoi d'un objet par le protocole http get
//  réponse si statut 200 : tableau d'objets
//  si status autre : objet "err"
const all_status = (req, res) => {
  Status.findAll({
  }).then((statuss) => {
    console.log(statuss);
    res.send(statuss);
  }).catch((err) => {
    console.log(err);
  })
};


//  récupérer un statut
const one_status = (req, res) => {
  const id = req.params.id;
  Status.findByPk(id, {
  }).then((status) => {
    console.log(status);
    res.send(status);
  }).catch((err) => {
    console.log(err);
  })
};


//  modifier un status
//  envoi d'un objet contenant les nouvelles données
//  réponse si status 201 : un objet "message"
//  sinon un objet "err"
const update_status = (req, res) => {
  const id = req.params.id;
  Status.update({
    status_type: req.body.status_type
  }, {
    where:  {
      status_id: id
    }
  }).then(() => {
    res.status(201).send({
      message: "Modifications enregistrées"
    }).catch((err) => {
      console.log(err);
    })
  })
};


//  exports des méthodes
exports.create_status = create_status;
exports.all_status = all_status;
exports.one_status = one_status;
exports.update_status = update_status;

