
const User = require('../models/userModel');
const Role = require('../models/roleModel');
//  pour utiliser bcrypt pour hasher le mdp
const bcrypt = require('bcrypt');
//  pour test : 10, en prod mettre plus de 12
const saltRounds = 10;

const jwt = require('jsonwebtoken');
//  emplacements des variables pour le token
const JWT_ENC = process.env.JWT_ENC;
const JWT_EXP = process.env.JWT_EXP;

//  pour pouvoir utiliser les opérateurs de Sequelize.Op :
const Sequelize = require('sequelize');
const Op = Sequelize.Op;


const all_users = (req, res) => {
  User.findAll({
    include: [{
      model: Role,
      required: true
    }], 
    order: [
      ['user_id', 'DESC']
    ],
    where: {
      is_active: 1
    }
  }).then((users) => {
    console.log(users);
    res.send(users);
  }).catch((err) => {
    console.log(err);
  })
};


//  get one_user by id
const one_user = (req, res) => {
  const id = req.params.id;
  User.findByPk(id, {
    include: [{
      model: Role,
      required: true
    }]
  }).then((user) => {
    console.log(user);
    res.send(user);
  }).catch((err) => {
    console.log(err);
  });
};

//  get user by username
//  requête select id from user 
//  where firstname = firstname and lastname = lastname
const user_by_username = (req, res) => {
  const fname = req.params.fname;
  const lname = req.params.lname;
  User.findOne({
    where :{
      [Op.and]: 
      [{
        firstname: fname
      }, {
        lastname: lname
      }]
    }
  }).then((user) => {
    console.log(user);
    res.send(user);
  }).catch((err) => {
    console.log(err);
  })
}


//  Créer un nouvel utilisateur, hasher son mdp 
//  et le stocker dans la bdd
const create_user = (req, res) => {
  const passwordsignup = req.body.password;
  console.log(passwordsignup);
  bcrypt.hash(passwordsignup, saltRounds, (err, hash) => {
    User.create({
      email: req.body.email,
      pwhash: hash, 
      firstname: req.body.firstname,
      lastname: req.body.lastname,
      fk_role_id: req.body.fk_role_id
  }).then(() => {
    // res.redirect('/')
    res.status(200).send({
      message: "Utilisateur enregistré"
    });
  }).catch((err) => {
    console.log(err);
  })
})};


//  Login : chercher un utilisateur via son adresse mail
//    si on la trouve on compare le hash du mdp avec celui stocké
//      si les hashes correspondent on renvoie message "ok"
//      si les hashes ne correspondent pas on renvoie une erreur (code mauvais login) 
//    si on ne la trouve pas on renvoie une erreur (code mauvais login)
const login = (req, res) => {
  const email = req.body.email;
  const pw = req.body.pwhash;
  console.log(email);
  console.log(pw);
  User.findOne({
    where: {
      email: email
    },
    include: [{
      model: Role,
      required: true
    }]
  }).then((user) => {
    console.log(`findOne user : ${JSON.stringify(user)}`);
    bcrypt.compare(pw, user.pwhash).then((result) => {
      const id = user.user_id;
      const role = user.role.title;
      const firstname = user.firstname;
      const lastname = user.lastname;
      const token = jwt.sign({email: user.email, pwhash: user.pwhash}, JWT_ENC, {expiresIn: JWT_EXP});
      res.json({id,token, role, firstname, lastname});
      console.log(token+' '+role);
      console.log(`role : ${user.role.title}`);
    }).catch((err) => {
      console.log(err);
    })
  });
};


//  update user :
//  envoi en put id dans le body
//  réponse si statut 201
//    objet message
//    objet erreur
const update_user = (req, res) => {
  const id = req.params.id;
  const passwordUpdate = req.body.password;
  console.log(passwordUpdate);
  bcrypt.hash(passwordUpdate, saltRounds, (err, hash) => {
    User.update({
      email: req.body.email,
      pwhash: hash,
      firstname: req.body.firstname,
      lastname: req.body.lastname,
      fk_role_id: req.body.fk_role_id
    }, {
      where: {
        user_id: id
      }
    })
  }).then(() => {
    res.status(201).send({
      message: "modifications enregistrées"
    }).catch((err) => {
      console.log(err);
    })
  })
};


//  Désactiver l'utilisateur : 
//  envoi de l'id en put pour l'updater (on passe is_active à 0)
//  Voir comment faire pour supprimer avec contraintes de clés étrangères
const deactivate_user = (req, res) => {
  const id = req.params.id;
  const is_active = req.body.is_active;
  User.findByPk(id).then((user) => {
    user.is_active = is_active;
    user.save();
    res.status(201).send({
      message: 'Modification enregistrée'
    })
  });
};
  // User.update({
  //   is_active: 0
  // }, {
  //   where: {
  //     user_id: id
  //   }
  // }).then(() => {
  //   res.status(200).send({
  //     message: "utilisateur désactivé"
  //   })
  // }).catch((err) => {
  //   console.log(err);
  // })


//  impossible de supprimer : voir comment supprimer FK en même temps
// const delete_user = (req, res) => {
//   const id = req.params.id;
//   User.destroy({
//     where: {
//       user_id: id
//     }
//   }).then(() => {
//     res.status(200).send({
//       message: "utilisateur supprimé"
//     })
//   }).catch((err) => {
//     console.log(err);
//   })
// };



/********************  exports des methodes ***************************/

exports.all_users = all_users;
exports.one_user = one_user;
exports.user_by_username = user_by_username;
exports.create_user = create_user;
exports.update_user = update_user;
exports.deactivate_user = deactivate_user;
exports.login = login;
// exports.delete_user = delete_user;

