
const mysql = require('mysql2');
const {Sequelize} = require('sequelize');

//  définition des variables de connexion à la bdd 
//  depuis le fichier .env
const DB_HOST = process.env.DB_HOST;
const BD_NAME = process.env.BD_NAME
const DB_USER = process.env.DB_USER;
const DB_PASS = process.env.DB_PASS;

const sequelize = new Sequelize(
    BD_NAME, 
    DB_USER, 
    DB_PASS, {
    host: DB_HOST,
    dialect: 'mysql'
});

module.exports = sequelize;
