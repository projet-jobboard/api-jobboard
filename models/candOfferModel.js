
const sequelize = require('../database');
const Sequelize = require('sequelize');

const candidateModel = require('./candidateModel');
const Candidate = require('./candidateModel');
const offerModel = require('./offerModel');
const Offer = require('./offerModel');


const CandOffer = sequelize.define('cand_offer', {
  candOffer_id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
  },
  fk_candidate_id: {
    type: Sequelize.INTEGER,
    allowNull: false,
    references: {
      model: candidateModel,
      key: 'candidate_id'
    }
  },
  fk_offer_id: {
    type: Sequelize.INTEGER,
    allowNull: false,
    references: {
      model: offerModel,
      key: 'offer_id'
    }
  },
  is_active: Sequelize.BOOLEAN
}, {
  timestamps: false,
  tableName: 'cand_offer'
});


//  définition des relations avec Candidate et Offer
CandOffer.hasOne(Candidate, {
  foreignKey: 'candidate_id',
  sourceKey: 'fk_candidate_id'
});

CandOffer.hasOne(Offer, {
  foreignKey: 'offer_id',
  sourceKey: 'fk_offer_id'
});


module.exports = CandOffer;
