
const sequelize = require('../database');
const Sequelize = require('sequelize');
const User = require('./userModel');


const Candidate = sequelize.define('candidate', {
  candidate_id: {
    tableName: 'candidate',
    type: Sequelize.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
  },
  fk_user_id: {
    type: Sequelize.INTEGER,
    references: {
      model: User,
      key: 'user_id'
    }
  },
  cv: {
    type: Sequelize.STRING,
    allowNull: true
  },
  is_active: {
    type: Sequelize.BOOLEAN,
    allowNull: true
  }
}, {
  timestamps: false,
  tableName: 'candidate',
  underscored: true
});

Candidate.hasOne(User, {
  foreignKey: 'user_id',
  sourceKey: 'fk_user_id'
});


module.exports = Candidate;
