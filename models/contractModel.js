
const sequelize = require('../database');
const Sequelize = require('sequelize');

const Contract = sequelize.define('contract', {
  contract_id: {
    type: Sequelize.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true
  },
  contract_type: {
    type: Sequelize.STRING,
    allowNull: false
  }
}, {
  timestamps: false,
  tableName: 'contract'
});

module.exports = Contract;
