
//  import infos de connexion et de bdd
const sequelize = require('../database');
const Sequelize = require('sequelize');

//  import modeles pour les relations
const Status = require('./statusModel');
const Sector = require('./sectorModel');
const User = require('./userModel');


const Enterprise = sequelize.define('enterprise', {
  enterprise_id: {
    type: Sequelize.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
  },  
  name: {
    type: Sequelize.STRING,
    allowNull: false
  },
  logo: {
    type: Sequelize.STRING,
    allowNull: true
  },
  description: {
    type: Sequelize.TEXT,
    allowNull: false
  },
  size: {
    type: Sequelize.STRING,
    allowNull: true
  },
  website: {
    type: Sequelize.STRING,
    allowNull: true
  },
  //  foreign key de la table sector
  fk_sector_id: {
    type: Sequelize.INTEGER,
    references: {
      model: Sector,
      key: 'sector_id'
    }
  },
  //  foreign key de la table status
  fk_status_id: {
    type: Sequelize.INTEGER,
    references: {
      model: Status,
      key: 'status_id'
    }
  },
  //  foreign key de la table user
  fk_user_id: {
    type: Sequelize.INTEGER,
    references: {
      model: User,
      key: 'user_id'
    }
  },
  is_active: {
    type: Sequelize.BOOLEAN,
    allowNull: true
  }
}, {
  timestamps: false,
  tableName: 'enterprise'
});

Enterprise.hasMany(Sector, {
  foreignKey: 'sector_id',
  sourceKey: 'fk_sector_id'
});

Enterprise.hasMany(Status, {
  foreignKey: 'status_id',
  sourceKey: 'fk_status_id'
});

Enterprise.hasOne(User, {
  // through: 'enterprise_user',
  foreignKey: 'user_id',
  sourceKey: 'fk_user_id'
});


module.exports = Enterprise;
