
const sequelize = require('../database');
const Sequelize = require('sequelize');
//  importer le model enterprise et la class Enterprise
const enterpriseModel = require('./enterpriseModel');
const Enterprise = require('./enterpriseModel');
//  importer le model contract et la class Contract
const Contract = require('./contractModel');
const contractModel = require('./contractModel');

const Sector = require('./sectorModel');
const sectorModel = require('./sectorModel');

const Status = require('./statusModel');
const statusModel = require('./statusModel');


//  déclarer et définir la table
const Offer = sequelize.define('offer', {
  offer_id: {
    type: Sequelize.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true
  },
  title: {
    type: Sequelize.STRING,
    allowNull: false
  }, 
  //  déclarer une clé étrangère (foreign key)
  fk_enterprise_id: {
    type: Sequelize.INTEGER,
    allowNull: false,
    references: {
      model: enterpriseModel,
      key: 'enterprise_id'
    }
  },
  detail: {
    type: Sequelize.TEXT,
    allowNull: false
  },
  recruitment_url: {
    type: Sequelize.STRING,
    allowNull: true
  },
  offer_location: {
    type: Sequelize.STRING,
    allowNull: true
  },
  xp_required: {
    type: Sequelize.STRING,
    allowNull: true
  },
  fk_contract_id: {
    type: Sequelize.INTEGER,
    allowNull: true,
    references: {
      model: contractModel,
      key: 'contract_id'
    }
  },
  fk_sector_id: {
    type: Sequelize.INTEGER,
    allowNull: true,
    references: {
      model: sectorModel,
      key: 'sector_id'
    }
  },
  fk_status_id: {
    type: Sequelize.INTEGER,
    allowNull: true,
    references: {
      model: statusModel,
      key: 'status_id'
    }
  }, 
  is_active: {
    type: Sequelize.BOOLEAN
  }
},
{
  //  empêcher la création des champs "created_at" et "updated_at"
  timestamps: false,
  //  déclaration du nom de la table (par défaut Sequelize le met au pluriel)
  tableName: 'offer'
});


//  déclaration des relations entre la classe Offer 
//  et les classes Enterprise, Contract, Sector et Status
Offer.hasOne(Enterprise, {
  foreignKey: 'enterprise_id',
  sourceKey: 'fk_enterprise_id'
});

Offer.hasOne(Contract, {
  foreignKey: 'contract_id',
  sourceKey: 'fk_contract_id'
});

Offer.hasOne(Sector, {
  foreignKey: 'sector_id',
  sourceKey: 'fk_sector_id'
});

Offer.hasOne(Status, {
  foreignKey: 'status_id',
  sourceKey: 'fk_status_id'
});



module.exports = Offer;
