
//  import infos de connexion et de bdd
const sequelize = require('../database');
const Sequelize = require('sequelize');


const Role = sequelize.define('role', {
  role_id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  title: {
    type: Sequelize.STRING,
    allowNull: false
  },
  description: {
    type: Sequelize.TEXT,
    allowNull: true
  },
  is_active: {
    type: Sequelize.BOOLEAN,
    allowNull: true
  } 
}, {
  timestamps: false,
  tableName: 'role'
});


module.exports = Role;
