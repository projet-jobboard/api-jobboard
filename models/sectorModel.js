
const sequelize = require('../database');
const Sequelize = require('sequelize');


const Sector = sequelize.define('sector', {
    sector_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    sector_name: {
        type: Sequelize.STRING,
        allowNull: false
    }
}, {
    timestamps: false,
    tableName: 'sector'
});


module.exports = Sector;
