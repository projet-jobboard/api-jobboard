
const sequelize = require('../database');
const Sequelize = require('sequelize');

const Staticpages = sequelize.define('staticpages', {
    static_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    title: {
        type: Sequelize.STRING,
        allowNull: false
    },
    content: {
        type: Sequelize.TEXT,
        allowNull: false
    },
    image: {
        type: Sequelize.STRING,
        allowNull: false
    }, 
    is_active: {
        type: Sequelize.BOOLEAN,
        allowNull: true
    }
}, {
    timestamps: false,
    tableName: 'staticpages'
});

module.exports = Staticpages;
