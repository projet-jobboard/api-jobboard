
const sequelize = require('../database');
const Sequelize = require('sequelize');

const Status = sequelize.define('status', {
    status_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    status_type: {
        type: Sequelize.STRING,
        allowNull: false,
    }
}, {
    timestamps: false,
    tableName: 'status'
});



module.exports = Status;
