
//  pour utiliser bcrypt pour hasher le mdp
const bcrypt = require('bcrypt');
const saltRounds = 10;  //  pour test : 10, en prod mettre plus de 12

//  import infos de connexion et de bdd
const sequelize = require('../database');
const Sequelize = require('sequelize');
const Role = require('./roleModel');
const Enterprise = require('./enterpriseModel');


const User = sequelize.define('user', {
    user_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false
    },
    pwhash: {
        type: Sequelize.TEXT,
        allowNull: false
    },
    firstname: {
      type: Sequelize.STRING,
      allowNull: false
    },
    lastname: {
      type: Sequelize.STRING,
      allowNull: false
    },
    fk_role_id: {
      type: Sequelize.INTEGER,
      allowNull: true,
      references: {
        model: Role,
        key: 'role_id'
      }
    },
    is_active: {
        type: Sequelize.BOOLEAN,
        allowNull: true
    }
}, {
    timestamps: false,
    tableName: 'user'
}
);

User.hasOne(Role, {
  foreignKey: 'role_id',
  sourceKey: 'fk_role_id'
});


module.exports = User;
