
const express = require('express');
const router = express.Router();
const candOfferController = require("../controllers/candOfferController");

const app = express();

//  route : /candOffer
router.post('/create', candOfferController.create_candOffer);
router.get('/all', candOfferController.all_candOffers);
router.get('/one/:id', candOfferController.one_candOffer)
router.put('/update/:id', candOfferController.update_candOffer);
router.put('/deactivate/:id', candOfferController.deactivate_candOffer);
// router.delete('/delete/:id', candOfferController.delete_candOffer);

//  canddiates by offer
router.get('/candidates_by_offer/:offer', candOfferController.candidates_by_offer);
router.get('/offers_by_candidate/:candidate', candOfferController.offers_by_candidate);


module.exports = router;
