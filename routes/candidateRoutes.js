
const express = require('express');
const app = express();
const router = express.Router();
const candidateController = require('../controllers/candidateController');

//  route : /candidate
router.post('/create', candidateController.create_candidate);
router.get('/all', candidateController.all_candidates);
router.get('/one/:id', candidateController.one_candidate);
router.get('/cand/:user', candidateController.cand_by_user);
router.put('/update/:id', candidateController.update_candidate);
router.put('/deactivate/:id', candidateController.deactivate_candidate);
// router.delete('/delete/:id', candidateController.delete_candidate);


module.exports = router;

