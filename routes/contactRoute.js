

//  import d'express
const express = require('express');

//  import contactController
const contactController = require('../controllers/contactController');

//  définition d'app pour utiliser express
const app = express();

//  middleWare d'express qui se base sur l'url de la requête
const router = express.Router();


router.post('/contact', contactController.main);





module.exports = router;
