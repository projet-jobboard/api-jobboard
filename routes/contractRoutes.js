
const express = require('express');
const router = express.Router();
const contractController = require('../controllers/contractController');


router.get('/all', contractController.all_contracts);
router.get('/one/:id', contractController.one_contract);
router.post('/create', contractController.create_contract);
router.put('/update/:id', contractController.update_contract);


module.exports = router;
