
const express = require('express');
const router = express.Router();
const enterpriseController = require("../controllers/enterpriseController");


//  route : /enterprise
router.get('/all', enterpriseController.all_enterprises);
router.get('/one/:id', enterpriseController.one_enterprise);
router.get('/user/:user_id', enterpriseController.ent_by_user);
router.post('/create', enterpriseController.create_enterprise);
router.put('/update/:id', enterpriseController.update_enterprise);
router.put('/deactivate/:id', enterpriseController.deactivate_enterprise);
//  router.delete('/delete/:id', enterpriseController.delete_enterprise);


//  get enterprises
router.get('/enterprises_by_sector/:sector', enterpriseController.enterprises_by_sector);
router.get('/enterprises_by_status/:status', enterpriseController.enterprises_by_status);

module.exports = router;
