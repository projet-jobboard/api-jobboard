
//  import d'express
const express = require('express');
//  middleWare d'express qui se base sur l'url de la requête
const router = express.Router();
//  import offerController
const offerController = require('../controllers/offerController');


//  route : /offer
router.get('/all', offerController.all_offers);
router.get('/one/:id', offerController.one_offer);
router.post('/create', offerController.create_offer);
router.put('/update/:id', offerController.update_offer);
router.put('/deactivate/:id', offerController.deactivate_offer);
//  router.delete('/delete_offer/:id', offerController.delete_offer);

//  offres par entreprise
router.get('/by_ent/:ent', offerController.offers_by_ents);

//  routes pour form de recherche
router.get('/by_contract/:contract', offerController.get_offers_by_contract);
router.get('/by_sector/:sector', offerController.get_offers_by_sector);
router.get('/by_status/:status', offerController.get_offers_by_status);
router.get('/by_keyword/:kw', offerController.get_offers_by_keyword);
router.get('/many_choices/:kw?/:contract?/:sector?/:status?', offerController.get_offers_many_choices);  

//  export routes
module.exports = router;
