
const express = require('express');

const pagesController = require('../controllers/StaticpagesController');

const app = express();

const router = express.Router();


router.get('/all', pagesController.all_pages);
router.get('/one/:id', pagesController.one_page);
router.post('/create', pagesController.create_page);
router.put('/update/:id', pagesController.update_page);
router.delete('/delete/:id', pagesController.delete_page);



module.exports = router;
