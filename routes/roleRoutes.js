
const express = require('express');
const router = express.Router();
const app = express();
const roleController = require('../controllers/roleController');


router.get('/all', roleController.all_roles);
router.get('/one/:id', roleController.one_role);
router.post('/create', roleController.create_role);
router.put('/update/:id', roleController.update_role);
router.put('/deactivate/:id', roleController.deactivate_role);



module.exports = router;
