
const express = require('express');
const router = express.Router();
const sectorController = require('../controllers/sectorController');


router.get('/all', sectorController.all_sectors);
router.get('/one/:id', sectorController.one_sector);
router.post('/create', sectorController.create_sector);
router.put('/update/:id', sectorController.update_sector);


module.exports = router;
