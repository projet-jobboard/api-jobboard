
const express = require('express');
const router = express.Router();
const app = express();
const userController = require('../controllers/userController');

router.post('/login', userController.login);

router.get('/all', userController.all_users);
router.get('/one/:id', userController.one_user);
router.get('/user_by_username/:fname/:lname', userController.user_by_username);
router.post('/create', userController.create_user);
router.put('/update/:id', userController.update_user);
router.put('/deactivate/:id', userController.deactivate_user);
// router.delete('/delete_user/:id', userController.delete_user);


module.exports = router;
